## Description ##
Number of codes in Python and Matlab to reproduce Figure 8
from Michailos et al., 2019 (Gcubed).

### Requirements:
- Python
- Matlab

### Order of codes to use with brief description
1) _quadtree_prep.py_

    Read quakeml catalog of earthquakes and 
    prepare input file for the Matlab quadtree code.

2) _quadtree_NZ.m_

    Runs quadtree. 

3) _read_matlab_output.py_

    Reads the output from the Matlab code and creates a file for 
    plotting with GMT (see folder named _gmt_map_)


