"""
Takes the qudtree outputs, bin edges, and rotate them back and convert Lat/Lon to
plot with GMT.

Te Whare wananga o te upoko o te Ika a Maui
VUW
March 2018
Author: K Michailos
"""

import csv
from pyproj import Proj, transform
import os
from obspy import read_events, Catalog
from pyproj import Proj, transform
from functions import *
import numpy as np

# Define working directory
WORK_DIR = os.getcwd()

boxes_csv_file = WORK_DIR + '/matlab/output/final_boxes.csv'

c_reader = csv.reader(open(boxes_csv_file, 'r'), delimiter=',')
X2 = list(zip(*c_reader))[1]
c_reader = csv.reader(open(boxes_csv_file, 'r'), delimiter=',')
X1 = list(zip(*c_reader))[0]
c_reader = csv.reader(open(boxes_csv_file, 'r'), delimiter=',')
Y1 = list(zip(*c_reader))[2]
c_reader = csv.reader(open(boxes_csv_file, 'r'), delimiter=',')
Y2 = list(zip(*c_reader))[3]

X1 = np.asarray([float(i) for i in X1])
X2 = np.asarray([float(i) for i in X2])
Y1 = np.asarray([float(i) for i in Y1])
Y2 = np.asarray([float(i) for i in Y2])


values_csv_file = WORK_DIR + '/matlab/output/final_values.csv'

c_reader = csv.reader(open(values_csv_file, 'r'), delimiter=',')
avdep_ = list(zip(*c_reader))[0]
avdep_ = np.asarray([float(i) for i in avdep_])


# # this here is optional it is just
# # to replace the zero values to 100...
avdep = []
for i in avdep_:

    if i == 0.0:
        val = 100
        avdep.append(val)
    else:
        avdep.append(i)


# Define the projections
inProj = Proj(init='epsg:4326')
outProj = Proj(init='epsg:2193')


# Define origin point
orig = [0, 0]
aaa = CoordRotator(orig, np.radians(-54))


for i, j in enumerate(X1):
    # print 'gmt psxy -R -J -Wthin,red -O -K  >> $out << END'
    aX, aY = aaa.inverse(X1[i], Y1[i])
    bX, bY = aaa.inverse(X2[i], Y2[i])
    cX, cY = aaa.inverse(X2[i], Y1[i])
    dX, dY = aaa.inverse(X1[i], Y2[i])

    cx, cy = transform(outProj, inProj, cX, cY)
    bx, by = transform(outProj, inProj, bX, bY)
    dx, dy = transform(outProj, inProj, dX, dY)
    ax, ay = transform(outProj, inProj, aX, aY)
    # Write boxes in a format readable by gmt
    with open(WORK_DIR + '/gmt_map/boxes_gmt.dat', 'a') as of:
        of.write('{}{}\n\
                  {} {}\n\
                  {} {}\n\
                  {} {}\n\
                  {} {}\n\
                  {} {}\n'.format('>-Z',
                  str(avdep[i]), cx, cy, bx, by, dx, dy, ax, ay, cx, cy))


