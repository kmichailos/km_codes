# miscallaneous


## RFs
* [RF](https://rf.readthedocs.io/en/latest/index.html) -  Python framework for receiver function analysis.
* [RfPy](https://paudetseis.github.io/RfPy/) - Python tools for calculating teleseismic receiver functions.
* [Pyraysum](https://paudetseis.github.io/PyRaysum/index.html) - Python wrapper around the Fortran software Raysum.
* [seispy](https://github.com/xumi1993/seispy) -  Python module for processing seismological data and calculating Receiver Functions.

Re-orient horizontal seismometers
* [OrientPy](https://github.com/nfsi-canada/OrientPy) - Seismic station orientation tools.


## Seismic Data Processing
* [ObsPy](http://docs.obspy.org/tutorial/) - seismic data handling, data gathering, and waveform processing
* [SAC](http://ds.iris.edu/ds/nodes/dmc/forms/sac/) - seismic data handling and waveform processing
* [SeisGo](https://github.com/xtyangpsp/SeisGo/tree/master/) - A ready-to-go Python toolbox for seismic data analysis


## Geophysics
* [SimPEG](http://docs.simpeg.xyz/index.html) - A python package for simulation and gradient based parameter estimation in the context of geophysical applications.
* [occamypy](https://github.com/fpicetti/occamypy) -An object-oriented optimization framework for small- and large-scale problems
* [devito](https://github.com/devitocodes/devito) - Fast Stencil Computation from Symbolic Specification
* [awesome-open-geoscience](https://github.com/softwareunderground/awesome-open-geoscience) - List of repositories that make our lives as geoscientists easier or just more awesome


## InSAR
* [InSAR](https://github.com/isce-framework)
* [Interferometric Synthetic Aperture Radar (InSAR) time series analysis](https://github.com/insarlab/MintPy)
* [More InSAR stuff](https://github.com/ASFOpenSARlab)
* [ASF](https://search.asf.alaska.edu/#/)
* [Time series analysis of InSAR data](https://github.com/yumorishita/LiCSBAS)
* [Comet webinars](https://www.youtube.com/channel/UCtFDytX1hgjvlS4NH48M2oQ/videos)
* [Nisar workshop August 30 - September 1](https://sites.google.com/view/nisarscience2022/home/)
* [European Ground Motion Service (EGMS)](https://land.copernicus.eu/pan-european/european-ground-motion-service)

### Visualization: On using color palletes 
* [Python-Graph-Gallery](https://www.python-graph-gallery.com/) - Graphs and plots
* [Perceptually Uniform Colour Maps - Peter Kovesi](https://peterkovesi.com/projects/colourmaps/)
* [The Rainbow Colour Map (repeatedly) considered harmful](https://blogs.egu.eu/divisions/gd/2017/08/23/the-rainbow-colour-map/)
* [Why rainbow colour scales can be misleading](https://www.climate-lab-book.ac.uk/2016/why-rainbow-colour-scales-can-be-misleading/)
* [More on rainbow pallete... ](https://doi.org/10.5194/hess-25-4549-2021)
* [Colour maps - Fabio Crameri](https://www.fabiocrameri.ch/)
* [Color maps](https://www.fabiocrameri.ch/colourmaps/)
* [Test figure is colorblind friendly](https://www.color-blindness.com/coblis-color-blindness-simulator/)
* [Plotting examples](https://github.com/geo7/plotting_examples#plots)
* [dataviz](https://clauswilke.com/dataviz/)

### Coding

### ML
* [Visual explanations of core machine learning concepts](https://mlu-explain.github.io/)

## Documents

### Gerneral Tools
* [Convertio](https://convertio.co/) - Convert your files to any formats (300+ formats supported)
* [WebPlotDigitizer](https://automeris.io/WebPlotDigitizer) - Web based tool to extract data from plots, images, and maps

### PDF
* [ILovePDF](https://www.ilovepdf.com/): Every tool you need to work with PDFs in one place
* [pdftk](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/): The PDF toolkit for doing everyday things with PDF documents
* [PDF2JPG](https://pdf2jpg.net/): Free PDF to JPG online converter
* [Smallpdf](https://smallpdf.com/): All-in-one easy-to-use online PDF tools

## Various stuff
* [Weather around the globe](https://earth.nullschool.net/) - See current wind, weather, ocean, and pollution conditions, as forecast by supercomputers, on an interactive animated map
* [Basic Geoscience reference card](https://static.squarespace.com/static/549dcda5e4b0a47d0ae1db1e/54a06d6ee4b0d158ed95f696/54a06d6fe4b0d158ed95fff0/1295033898443/Cheatsheet_basic.pdf) 
* [Geophysics reference card](https://static.squarespace.com/static/549dcda5e4b0a47d0ae1db1e/54a06d6ee4b0d158ed95f696/54a06d70e4b0d158ed9603f5/1350658645407/Cheatsheet_geophysics.pdf) 
* [Rock Physics reference card](https://static.squarespace.com/static/549dcda5e4b0a47d0ae1db1e/54a06d6ee4b0d158ed95f696/54a06d6fe4b0d158ed960042/1374593568367/Cheatsheet_Rock_Physics.pdf) 
* [Re-thinking research assessment](https://sfdora.org/resource/rethinking-research-assessment-for-the-greater-good/)


Books
* [New Manual of Seismological Observatory Practice (NMSOP-2)](https://bib.telegrafenberg.de/publizieren/bibliotheksverlag/nmsop)
* [An Introduction to Seismology, Earthquakes, and Earth Structure](http://epsc.wustl.edu/seismology/book/presentations/2019/Stein&Wysession.pdf)
* [Figures](https://levee.wustl.edu/seismology/book/) from An Introduction to Seismology, Earthquakes, and Earth Structure


Alpine Fault and NZ in general
---
* [RNZ podcast](https://www.rnz.co.nz/national/programmes/ourchangingworld/audio/2018822479/listening-to-the-hum-of-the-alpine-fault)
* [YT link 1](https://www.youtube.com/watch?v=bU_O6Qe6Knk)
* [YT link 2](https://www.youtube.com/watch?v=5fUj7zjbAFw)
* [YT link 3](https://www.youtube.com/watch?v=ohVdKx8PDGY)
* [YT link 4](https://www.youtube.com/watch?v=mpAWLz4W1Go) - How mountains are destroyed ...
* [Kaikoura earthquake](https://www.wgtn.ac.nz/sgees/about/news/news-archives/2016-news/victoria-scientists-respond-to-a-quake-hit-new-zealand)

