% Resample hypoDD residuals (hypoDD.res) 
% create N dt.cc and dt.cc
%%%%% MM 11/2016 %%%%%%%%%%%
clear
clc

tic
%disp('Default number of samples set to 200')
% Set number of samples 
N=200;

%% 1.Load hypoDD.res file 
%Input format 
% STATION - DT - ID1 - ID2 - IDX - WEIGHT - RES - WT - OFFS

filename='hypoDD.res';
fin=fopen(filename);
a=textscan(fin,'%s %f %f %f %f %f %f %f %f \n');
fclose(fin);

clear ans filename fin 
%% 2. Define residuals 
%Define Parameters
residuals=a{:,7};
idx=a{:,5};

%CC and catalog pairs
ind1=find(idx(:,1)==1 | idx(:,1)==2);
ind2=find(idx(:,1)==3 | idx(:,1)==4);

%catalog and cc data
%station name - differential time (sec)- id1 id2 - phase
for i=1:5
a_cc{1,i}=a{i} (ind1); 
a_ct{1,i}=a{i} (ind2); 
end
%residuals for cc and ct
cc_res=residuals(ind1);
ct_res=residuals(ind2);

clear i
% create histograms for visual inspection (optional)
%figure
%histogram(ct_res,min(ct_res):10:max(ct_res))
%figure
%histogram(cc_res,-50:1:50)

%% 3. resample
%residuals in ms
for i=1:N
cc(:,i)=randsample(cc_res,length(cc_res),true); 
ct(:,i)=randsample(ct_res,length(ct_res),true);
end

%convert ms to sec
cc=cc./1000;
ct=ct./1000;
clear ans i idx ind1 ind2 

%% 4. Create N dt.cc files
cc2hypoDD(a_cc,cc,N);

%% 5. create N dt.ct files
%load dt.ct file  ---> Do not load dt.ct file
%create a new one from hypoDD.res 
% format:
%# id1 id2 
%sta dt+res 0.0 weight phase
ct2hypoDD(a_ct,ct,N);   

sprintf('Elapsed Time: %f minutes',toc/60)
