### Order of codes to use for bootstrap analysis. Matlab codes are written by M. Mesimeri.

1) _bootstrap_hypoDD.m_
 
    Create the synthetic dt.cc and dt.ct files;
    N=200 will create 200 folders  

2) Run hypoDD for each of the N bootstraps using the following bash code:

    Note: Need to change the files that are being copied below (e.g., I use a 3D velocity model here)
    to match the ones you use for running HypoDD.

```
#!/bin/bash
for file in {1..50..1}; do echo $file; cd $file
cat >  sample_$file.sh <<EOF
#Copy hypoDD files
 cp -v ../hypoDD.sta .; cp -v ../hypoDD.inp .; cp -v ../event.sel .; cp -v ../Vp_model_p.dat .; cp -v ../hypoDD .; cp -v ../station.sel .;
#Run hypoDD 
./hypoDD  hypoDD.inp 
#create catalog
cp hypoDD.reloc sample_$file.txt

cp -v *$file.txt ../LOCATIONS
# rm -v hypo*
# rm -v dt.ct
# rm -v dt.cc
EOF
  chmod +x  sample_$file.sh; sh sample_$file.sh & cd ../; done 
```

3) _bootstrap_hypoDD_plots.m_
    
    Calculates Sx, Sy, Sz and gives a txt file of the Sx, Sy, Sz.
    Modified the matlab code so it gives ID, Sx, Sy and Sz. ID is the id number used by 
    HypoDD for matching initial locations to relocations. 

4. _HDD2quakeml.py_
    
    Read hypoDD files, uncertainties file and quakeML file with locations that were input 
    for HypoDD. Adds a new origin in the quakeML file.