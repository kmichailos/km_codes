%Plot uncertainties (bootstraping)
clear; clc

%%% Open parallel tool
% matlabpool('local',4)

tic

%% 1.Load Files
disp('Loading files...')

N=200;
%load final locations
ini=load('hypoDD.reloc');
%flag 9999 means that is the final hypoDD file
ini=[ini(:,1:23) 9999*ones(length(ini),1)];
k=ini(end,1);

% change directory work in LOCATIONS
cd LOCATIONS
%load catalogs
for i=1:N
filename=sprintf('sample_%d.txt',i);
disp(filename)
a=load(filename); 
A{1,i}=a;   
end
%return to working directory
cd ../

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 2. Find common earthquakes
disp('Common events....')
%one matrix
B=vertcat(A{:});
B=[B;ini];
j=1;
%common events
for i=1:k
 c=B(B(:,1)==i,:);
 if length(c(:,1)) == N +1 ;
 common{j,1}=c;
 j=j+1;
 end
end
clear i B j

%% 3. 95% ellipsoid -- plots and std
mkdir XY
mkdir Z

%X Y 
%Parallel!!
for i=1:length(common)
disp(sprintf('Event...: %d  out of %d ',i, length(common)))     
temp=common{i,1};

%%%%%%%%%%%%%%%%%%%%  FILENAMES %%%%%%%%%%%%%%%%%%
filename=sprintf('Event: %d',temp(1,1));
filename2=sprintf('Z_event_%d.png',temp(1,1));
filename3=sprintf('XY_event_%d.png',temp(1,1));

%%%%%%%%%%%%%%%% depths in METERS!!!!!!! %%%%%%
depth=(temp(end,4)-temp(1:end-1,4))*1000;

%%%%%%%%%%%% X direction in METERS !!!!!!!! %%%%%%%%%%%%%%%%%%%%%%
X_axis=(temp(end,3)-temp(1:end-1,3)).*(cos(temp(1:end-1,2)*pi/180))*111.11*1000;

%%%%%%%%%%% Y Direction in METERS !!!!!%%%%%%%%%%%%%%%%%%%%%%%
Y_axis=(temp(end,2)-temp(1:end-1,2))*111.11*1000;

%calculate ellipse for X Y
[sx,sy,rxy_ellipse,inxy_points, outxy_points]=error_ellipse(X_axis,Y_axis);

Sx(i,1)=sx;
Sy(i,1)=sy;

%%%%%%%%%%%%%%       PLOT  X-Y  %%%%%%%%%
f1=figure('visible','off');
%points inside ellipse
plot(inxy_points(:,1),inxy_points(:,2),'k+')
axis square
grid minor
hold on 
%points outside ellipse
plot(outxy_points(:,1),outxy_points(:,2),'r+')
%Draw the error ellipse
plot(rxy_ellipse(:,1) + mean(X_axis),rxy_ellipse(:,2) + mean(Y_axis),'-')
hold on;
ylabel('Distance [m]','FontSize',12)
xlabel('Distance [m]','FontSize',12)
hold off 
title(filename,'FontSize',13)
% saveas(f1,filename3,'png')

%calculate ellipse for X Z
[sxx,sz,rxz_ellipse,inxz_points, outxz_points]=error_ellipse(X_axis,depth);
Sz(i,1)=sz;

ID(i,1)=temp(end,1);

%%%%%%%%%%%%%%%%%%%%% PLOT  -- Z %%%%%%%%%%%%%%%%%%%%
f2=figure('visible','off');
%points inside ellipse
plot(inxz_points(:,1),inxz_points(:,2),'k+')
axis square
hold on
%points outside ellipse
plot(outxz_points(:,1),outxz_points(:,2),'r+')
%Draw the error ellipse
plot(rxz_ellipse(:,1) + mean(X_axis),rxz_ellipse(:,2) + mean(depth),'-')
hold on;
ylabel('Distance [m]','FontSize',12)
grid minor
title(filename,'FontSize',13)
% saveas(f2,filename2,'png')
 
% movefile(filename2,'Z/')
% movefile(filename3,'XY/')
close all
end

%% 4. Calculate median in meters !!
%median
mdx=median(Sx);
mdy=median(Sy);
mdz=median(Sz);

perdz=prctile(Sy,90)

a = [ID, Sx, Sy, Sz]
csvwrite('bootstrap_uncertainties.csv',a)

save bootstrap_hypoDD.mat
sprintf('Elapsed Time: %f minutes',toc/60)

% delete(gcp)
