function   cc2hypoDD( a_ct, ct, N )
%Create N dt.cc 

%define parameters
sta=a_ct{1,1};
i=a_ct{1,3};
j=a_ct{1,4};
dt=a_ct{1,2};
dt2=zeros(length(dt),1);
wgh=ones(length(dt),1);
%label id
ind=sub2ind([max(i) max(j)],i,j);
pha=a_ct{1,5};

%convert 1 and 2 to P and S
phastr=cell(size(pha));
phastr(pha==3)={'P'};
phastr(pha==4)={'S'};

indold =0;
 
%start writing output files
for k=1:N
% create folder for each output file    
disp(sprintf('Sample (dt.ct)...: %d  out of %d ',k, N))
filename=sprintf('%d',k);
cd(filename);
ndt=dt+ct(:,k);
fid=fopen('dt.ct','w');
%writing output (same code with correl2hypoDD)
for l=1:length(dt)
  if ind(l)~=indold
    fprintf(fid,'# %9d  %9d  \n',i(l),j(l));
    indold=ind(l);
  end
     fprintf(fid,'%7s  %9.5f %9.5f %4.2f  %s\n',sta{(l)},ndt(l),dt2(l),wgh(l), ...
     phastr{l});
end
fclose(fid);
cd ../
clear ndt
 end
end

