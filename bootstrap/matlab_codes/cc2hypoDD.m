function   cc2hypoDD( a_cc, cc, N )
%Create N dt.cc 

%define parameters
sta=a_cc{1,1};
i=a_cc{1,3};
j=a_cc{1,4};
dt=a_cc{1,2};
wgh=ones(length(dt),1);
%label id
ind=sub2ind([max(i) max(j)],i,j);
pha=a_cc{1,5};

%convert 1 and 2 to P and S
phastr=cell(size(pha));
phastr(pha==1)={'P'};
phastr(pha==2)={'S'};

indold =0;
 
%start writing output files
for k=1:N
% create folder for each output file   
disp(sprintf('Sample (dt.cc)...: %d  out of %d ',k, N))
filename=sprintf('%d',k);
mkdir(filename);
cd(filename);
ndt=dt+cc(:,k);
fid=fopen('dt.cc','w');
%writing output (same code with correl2hypoDD)
for l=1:length(dt)
  if ind(l)~=indold
    fprintf(fid,'# %9d  %9d  0\n',i(l),j(l));
    indold=ind(l);
  end
     fprintf(fid,'%7s  %9.5f  %4.2f  %s\n',sta{(l)},ndt(l),wgh(l), ...
     phastr{l});
end
fclose(fid);
cd ../
clear ndt
 end
end

