"""
Adds the hypoDD origin to an existing catalog including bootstrap uncertainties.

Requires:
* HypoDD run files
* bootstrap_uncertainties.csv
* original quakeml file (NOTE: last origin of the quakeml files should be the location
                               used for input in HypoDD)

:authors: KM
:date: 15/06/2018
"""

from obspy import read_events, Catalog
from obspy.core import UTCDateTime
import os
import glob
import obspy
from obspy.io.nordic.core import read_nordic
import csv


# Define working directory
WORK_DIR = os.getcwd()

#############################################################################
#READ NLL CAT
#############################################################################
# ***************************************************************************
# READ QUAKEML FILE
print('>>> Reading NonLinLoc events and creating a catalog...')
# TODO: fill in the rest of the path
CAT_PATH = WORK_DIR + ''
nlloc_cat = read_events(CAT_PATH)
print('>>> Read NonLinLoc events and created a catalog...')

# Add 'NLLoc' to the event origin description
for event in nlloc_cat:
    event.origins[-1].creation_info.version = 'NLLoc'
#############################################################################
# Read event.dat
# *****************************************
print('>>> Reading all locations...')
working_dir =(WORK_DIR + '/matlab_codes/')
event_loc = os.path.join(os.path.join(working_dir,"event.dat"))
# ***************************************************
eq_id = []
with open(event_loc, "r") as open_file:
    for line in open_file:
        print(line)
        ln = line.split(' ')
        eq_id.append(str(int(ln[-1])))
eq_id = [int(i) for i in eq_id]
#############################################################################
print('>>> Add ID of events...')
for i, event in enumerate(nlloc_cat):
    # add the event id as a comment in the nll_event
    event.comments.append(obspy.core.event.Comment(
        text="%s" % str(eq_id[i])))
############################################################################
# **************************************************************************
print('>>> Reading HypoDD relocations...')
hypodd_reloc = os.path.join(os.path.join(working_dir,"hypoDD.reloc"))
# ***************************************************************************
reloc_id = []
reloc_origin = []
reloc_latitude = []
reloc_longitude = []
reloc_depth = []
with open(hypodd_reloc, "r") as open_file:
    for line in open_file:
        event_id, lat, lon, depth, _, _, _, _, _, _, year, month, \
            day, hour, minute, second, _, _, _, _, _, _, _, \
            cluster_id = line.split()
        reloc_id.append(event_id)
        reloc_lat, reloc_lon, reloc_dep = map(float, [lat, lon, depth])
        # Convert back to meters.
        reloc_dep *= 1000.0
        sec = int(float(second))
        # Correct for a bug in hypoDD which can write 60 seconds...
        add_minute = False
        if sec >= 60:
            sec = 0
            add_minute = True

        reloc_orig = UTCDateTime(int(year), int(month), int(day),
                                 int(hour), int(minute), sec,
                                 int((float(second) % 1.0) * 1E6))
        if add_minute is True:
            reloc_orig = reloc_orig + 60.0
        reloc_latitude.append(reloc_lat)
        reloc_longitude.append(reloc_lon)
        reloc_depth.append(reloc_dep)
        reloc_origin.append(reloc_orig)
reloc_id = [int(i) for i in reloc_id]
print('>>> Read HypoDD relocations...')

print('>>> Reading HypoDD relocations...')
for i, event in enumerate(nlloc_cat):
    try:
        # print nll_event.origins[0].time, nll_event.comments[1]
        for j, id in enumerate(reloc_id):
            if str(id) == event.comments[-1].text:
                print(str(id), event.comments[-1].text)
                print(reloc_origin[j], event.origins[0].time)
                event.origins.append(obspy.core.event.Origin())
                event.origins[-1].time = reloc_origin[j]
                event.origins[-1].longitude = reloc_longitude[j]
                event.origins[-1].latitude = reloc_latitude[j]
                event.origins[-1].depth = reloc_depth[j]
                event.origins[-1].creation_info = ''
                event.origins[-1].creation_info.version = 'HypoDD'
    except Exception as e:
        print(e)
        continue

print('>>> Read HypoDD bootstrap uncertainties...')
hdd_unc = os.path.join(os.path.join(working_dir,"bootstrap_uncertainties.csv"))
unc_id = []
ver_unc = []
lat_unc = []
lon_unc = []
with open(hdd_unc, "r") as f:
    for line in f:
        ln = line.split(',')
        unc_id.append(ln[0])
        ver_unc.append(float(ln[1]))
        lat_unc.append(float(ln[2]))
        lon_unc.append(float(ln[3]))


print('>>> Match HypoDD bootstrap uncertainties...')
for i, event in enumerate(nlloc_cat):
    if event.origins[-1].creation_info.version == 'HypoDD':
        ev_id = event.comments[-1].text
        print(event)
        for k, unc_id_ in enumerate(unc_id):
            if ev_id == unc_id_:
                event.origins[-1].depth_errors.uncertainty = ver_unc[k]
                event.origins[-1].latitude_errors.uncertainty = lat_unc[k]
                event.origins[-1].longitude_errors.uncertainty = lon_unc[k]

nlloc_cat.write(WORK_DIR + '/*_HDD.xml', format='QUAKEML')
