### GMT5 and Python codes used for plots included in the first chapter of my PhD thesis

Figure 1: Map of seismic sites

Figure 2: Automatic vs manual phase picks

Figure 3: Magnitude frequency distribution

Figure 4: Location uncertainties

Figure 5: Epicenters location map

Figure 6: Cross sections across the strike of the AF

Figure 7: Cross sections along the strike of the AF

Figure 8: Boxes of seismogenic cutoff depths map