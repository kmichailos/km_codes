# Focal mechanisms and stress inversions

Description
------------
Set of codes for reproducing the results and plots from the manuscript titled
"Detailed spatiotemporal analysis of the tectonic stress regime near 
the central Alpine Fault, New Zealand" published in [Tectonophysics](https://www.sciencedirect.com/science/article/abs/pii/S0040195119303208?via%3Dihub).


How to run the codes
------------
WIP


Note
------------
Codes here only reproduce our results in the specific publication.
For different applications the codes will need to be modified.


