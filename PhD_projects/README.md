This subdirectory contains scrips in python and GMT (for mapping) that recreate plots and 
results from my PhD projects into two separate folders:
- Earthquake_detections_locations
- Focal_mechanisms_stress

The scripts for a third project from my PhD work can be found in a separate
GitHub repository. To access the repository click [here](https://github.com/kemichai/exhumation-rates-modeling).